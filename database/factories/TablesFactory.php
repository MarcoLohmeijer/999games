<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Table;
use App\User;
use Faker\Generator as Faker;

$factory->define(Table::class, function (Faker $faker) {
    $maxUser = count(User::all());
    return [
        'number' => $faker->randomElement(array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)),
        'user_id' => $faker->numberBetween(1, $maxUser),
    ];
});
