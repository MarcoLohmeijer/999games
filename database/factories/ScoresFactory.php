<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\player;
use App\User;
use App\rounds;
use App\Score;

use Faker\Generator as Faker;

$factory->define(Scores::class, function (Faker $faker) {
    $maxPlayer = count(Player::all());
    $maxRound = count(rounds::all());
    $maxUser = count(User::all());

    return [
        'points'=>$faker->numberBetween(1-100),
        'weight'=>$faker->numberBetween(1,50),
        'payer_id'=>$faker->numberBetween(1, $maxPlayer),
        'round_id'=>$faker->numberBetween(1, $maxRound),
        'user_id'=>$faker->numberBetween(1,$maxUser)
    ];
});
