<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Player;
use Faker\Generator as Faker;

$factory->define(Player::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'code' => $faker->randomNumber(),
        'email' => $faker->unique()->safeEmail,
        'phonenumber' => $faker->randomNumber(9),
        'checked_in' => $faker->boolean,
        'user_id' => factory('App\User')->create()->id
    ];
});
