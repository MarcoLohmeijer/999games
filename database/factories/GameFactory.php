<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\GameRule;
use App\User;
use Faker\Generator as Faker;

$factory->define(Game::class, function (Faker $faker) {
    $maxGameRule = count(GameRule::all());
    $maxUserId = count(User::all());
    return [
        'name' => $faker->firstName(),
        'description' => $faker->text(),
        'user_id' => $faker->numberBetween(1, $maxUserId)
    ];
});
