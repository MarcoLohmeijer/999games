<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GameRule;
use App\User;
use Faker\Generator as Faker;

$factory->define(GameRule::class, function (Faker $faker) {
    $maxUser = count(User::all());
    return [
        'max_players' => $faker->numberBetween(4,10),
        'min_players' => $faker->numberBetween(2,4),
        'preferred_players' => $faker->numberBetween(2,10),
        'time' => $faker->randomElement(array(600,1200,1800,2400,3000,3600)),
        'knockout_phase' => $faker->randomElement(array(16, 32,64)),
        'user_id' => $faker->numberBetween(1, $maxUser),
        'game_id' => $faker->numberBetween(1, 10)
    ];
});
