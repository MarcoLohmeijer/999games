<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use App\Round;
use App\Rounds;
use App\User;
use Faker\Generator as Faker;

$factory->define(Round::class, function (Faker $faker) {
    $maxGameID = count(Game::all());
    $maxUserId = count(User::all());
    return [
        'name' => $faker->word(),
        'time' => $faker->randomElement(array(600,1200,1800,2400,3000,3600)),
        'start_time' => $faker->unixTime(),
        'game_id' => $faker->numberBetween(1, $maxGameID),
        'user_id' => $faker->numberBetween(1,$maxUserId),
];
});
