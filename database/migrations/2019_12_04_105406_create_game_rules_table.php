<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('max_players');
            $table->integer('min_players');
            $table->integer('max_rounds');
            $table->integer('knockout_phase');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('game_id')->unsigned()->index();
            $table->foreign('game_id')->references('id')->on('games');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_rules');
    }
}
