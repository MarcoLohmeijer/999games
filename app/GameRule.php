<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class GameRule extends Model
{
    /**
     * @return BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id');
    }
}
