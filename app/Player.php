<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Player extends Model
{
    /**
     * @return BelongsToMany
     */
    public function tables()
    {
        return $this->belongsToMany(Table::class, "table_players");
    }

    /**
     * @return HasMany
     */
    public function scores()
    {
        return $this->hasMany(Score::class);
    }

    /**
     * @return BelongsToMany
     */
    public function games()
    {
        return $this->belongsToMany(Game::class, 'player_games');
    }
}
