<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Game extends Model
{
    /**
     * @return HasOne
     */
    public function gameRules()
    {
        return $this->hasOne(GameRule::class);
    }

    /**
     * @return HasMany
     */
    public function playerGames()
    {
        return $this->hasMany(Player_games::class);
    }

    /**
     * @return HasMany
     */
    public function tables()
    {
        return $this->hasMany(Table::class);
    }

    /**
     * @return HasMany
     */
    Public function rounds()
    {
        return $this->hasMany(Round::class);
    }

    /**
     * @return BelongsToMany
     */
    public function players()
    {
        return $this->belongsToMany(Player::class, 'player_games')->withPivot('weight', 'leaderboard_points');
    }

}
