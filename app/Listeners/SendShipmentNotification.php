<?php

namespace App\Listeners;

use App\Events\OrderShiped;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailer;
use App\Mail\OrderShipped;

class SendShipmentNotification
{
    /** @var Mailer */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  OrderShiped  $event
     * @return void
     */
    public function handle(OrderShiped $event)
    {
        $this->mailer->send(new OrderShipped($event->getPlayer()));
    }
}
