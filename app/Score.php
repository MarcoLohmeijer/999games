<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Score extends Model
{
    /** @var array */
    protected $fillable = [
        'points', 'weight', 'player_id', 'round_id', 'user_id'
    ];

    /**
     * @return BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id') ;
    }
}
