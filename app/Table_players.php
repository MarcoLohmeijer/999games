<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class table_players extends Model
{
    /** @var array */
    protected $fillable = [
        'player_id', 'table_id', 'round_id',
    ];

    /**
     * @return BelongsTo
     */
    public function rounds()
    {
        return $this->belongsTo(Round::class);
    }
}
