<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Round extends Model
{
    /**
     * @return BelongsToMany
     */
    public function tables()
    {
        return $this->belongsToMany(Table::class);
    }

    /**
     * @return BelongsToMany
     */
    public function table_players()
    {
        return $this->belongsToMany(Table_players::class);
    }

    /**
     * @return HasMany
     */
    public function scores()
    {
        return $this->hasMany(Score::class);
    }
}
