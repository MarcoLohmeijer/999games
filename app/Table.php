<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Table extends Model
{
    /**
     * @return BelongsToMany
     */
    public function players()
    {
        return $this->belongsToMany(Player::class, "table_players");
    }

    /**
     * @return BelongsTo
     */
    public function rounds()
    {
        return $this->belongsTo(Round::class);
    }

    /**
     * @return BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}
