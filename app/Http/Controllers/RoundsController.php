<?php

namespace App\Http\Controllers;

use App\Game;
use App\GameRule;
use App\Round;
use App\Player;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Route;
use App\Http\Requests\RoundStoreRequest;
use Illuminate\View\View;

class RoundsController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $rounds = Round::all();
        return view('rounds.index', compact('rounds'));
    }

    /**
     * @param Game $game
     * @return Factory|View
     */
    public function showRounds(Game $game)
    {
        $rounds = Round::where('game_id', '=', $game->id)->paginate(6);
        return view('rounds.index', compact('rounds', 'game'));
    }

    /**
     * Create function
     */
    public function create()
    {
        //
    }

    /**
     * @return Factory|View
     */
    public function storeRoundForm()
    {
        return view('rounds.storeForm');
    }

    /**
     * @param RoundStoreRequest $request
     */
    public function store(RoundStoreRequest $request)
    {
        //
    }

    /**
     * @param Request $request
     * @param Game $game
     * @param GameRule $gameRules
     *
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function storeRound(Request $request, Game $game, GameRule $gameRules)
    {
        $gameRules = GameRule::where('id', $game->id)->first();
        $hoursInSec = (int)substr($request->get('time'),0 ,2) * 3600;
        $minutesInSec = (int)substr($request->get('time'),3 ,2) * 60;
        $time = $hoursInSec + $minutesInSec;
        $startTime = strtotime($request->get('start_time'));

        if (count($game->rounds()->get()) < $gameRules['max_rounds']) {
            $round = new Round();
            $round->name = $request->get('name');
            $round->time = $time;
            $round->start_time = $startTime;
            $round->game_id = $game->id;
            $round->user_id = $request->user()->id;
            $round->save();
            return redirect('/game/'.$round->game_id.'/round/'.$round->id.'/tables')->with('success','Round created successfully!');
        }
        return view('/confirmation')->with('msg','Cant create a new Round. Round limit reached!!!');
    }

    /**
     * @param Request $request
     * @param Game $game
     *
     * @return RedirectResponse
     */
    public function storeKnockoutRound(Request $request, Game $game)
    {
        $hoursInSec = (int)substr($request->get('time'),0 ,2) * 3600;
        $minutesInSec = (int)substr($request->get('time'),3 ,2) * 60;
        $time = $hoursInSec + $minutesInSec;
        $startTime = strtotime($request->get('start_time'));

        count($game->rounds()->get());
        $round = new Round();
        $round->name = $request->get('name');
        $round->time = $time;
        $round->start_time = $startTime;
        $round->game_id = $game->id;
        $round->user_id = $request->user()->id;
        $round->save();
        return redirect()->action('KnockoutController@getKnockoutPlayers', [$game->id, $round->id])->with('success', 'Knockout-out round created successfully!');
    }


    /**
     * @param Round $round
     * @return Factory|View
     */
    public function show(Round $round)
    {
        return view('rounds.showOneRound', compact('round') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Round $round
     */
    public function edit(Round $round)
    {
        //
    }

    /**
     * @param Request $request
     * @param Round $round
     *
     * @return RedirectResponse
     */
    public function update(Request $request, Round $round)
    {
        $round->name = $request->get('name');
        $round->time = $request->get('time');
        $round->start_time =$request->get('start_time');
        $round->game_id = $request->get('game_id');
        $round->user_id = $request->get('user_id');

        $round->save();

        return redirect()->back()->with('info','Round updated successfully!');
    }

    /**
     * @param Round $round
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Round $round)
    {
        $round->delete();
        return redirect()->back()->with('warning','Round deleted successfully!');
    }

    /**
     * @param Round $round
     *
     * @return array
     */
    public function getOneRound(Round $round)
    {
        return compact('round') ;
    }
}
