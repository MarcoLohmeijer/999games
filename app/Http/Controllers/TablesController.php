<?php

namespace App\Http\Controllers;

use App\Game;
use App\Player_games;
use App\Round;
use App\Score;
use App\player;
use App\Table;
use App\table_players;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TablesController extends Controller
{
    /**
     * @param Round $round
     * @return Factory|View
     */
    public function index(Round $round)
    {
        $tables = Table::where('round_id', '=', $round->id);
        return view('tables.tableoverview', compact('tables', 'game'));
    }

    /**
     * @param Game $game
     * @param Round $round
     * @return Factory|View
     */
    public function showTables(Game $game, Round $round)
    {
        $scores = Score::where('round_id','=',$round->id)->get();
        $tables = Table::where('round_id','=', $round->id)->get();
        foreach ($tables as &$table) {
            $table->players = $table->players()->get();
            $total = 0;
            foreach ($table->players as &$player) {
                $score  = $scores->firstWhere('player_id', $player->id);
                $points = $score ? $score->points : 0;
                $weight = $score ? $score->weight : 0;
                $total += $points;
                $player->setAttribute('points', $points);
                $player->setAttribute('weight', $weight);
            }
            $table->setAttribute('points', $total);
        }
        return view('tables.tableoverview', compact('tables','game', 'round'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Game $game
     * @param Round $round
     * @return RedirectResponse
     */
    public function storeTable(Request $request, Game $game, Round $round)
    {
        $max_round = Round::where('game_id', '=', $game->id)->max('id');
        if($max_round != $round->id)
        {
            return back()->with('error','De vorige ronde is nog niet afgelopen!!!');
        }

        $tables = Table::where('round_id', '=', $round->id)->delete();
        $player = Player::where('state', '=', 1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->update(['state' => 0]);

        // gets all players with checked_id=0 for current game_id
        $players = Player::whereRaw('state=0')->where('checked_in','=',1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->orderBy('player_games.weight', 'desc')
            ->inRandomOrder()->get();

        $cnt = count($players);
        $num = 1;
        $plr = 0;

        while ($cnt >= 3) {
            //$tbl = $cnt == 3 || $cnt == 6 || $cnt == 9 ? 3 : 4;
            if ($cnt == 3 || $cnt == 6)
            {
                $tbl = 3;
            }
            elseif ($cnt == 5)
            {
                $tbl = 5;
            }
            else
            {
                $tbl = 4;
            }

            $table = new Table;
            $table->number = $num;
            $table->game_id = $game->id;
            $table->round_id = $round->id;
            $table->user_id = $request->user()->id;
            $table->save();

            for ($i = 0; $i < $tbl; $i++, $plr++) {
                $player = $players[$plr];

                $tp = new table_players;
                $tp->player_id = $player->id;
                $tp->table_id = $table->id;
                $tp->round_id = $round->id;
                $tp->save();

                $player->state = 1;
                $player->save();
            }

            $cnt -= $tbl;
            $num++;

        }
        return back()->with('success','Player added to table successfully!');
    }

    /**
     * @param Game $game
     *
     * @return RedirectResponse
     */
    public function store(Game $game)
    {
        $latestTable = $game->tables()->latest()->first();
        $table = new Table();
        $table->game_id = $game->id;
        $table->user_id = Auth::user()->id;
        if ($latestTable) {
            $number = $latestTable->number + 1;
            $table->number = $number;
            $table->save();
            return back();
        }

        $table->number = 1;
        $table->save();

        return back();
    }

    /**
     * Display the specified resource.
     * @param Table $table
     *
     * @return void
     */
    public function show(Table $table)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param Table $table
     *
     * @return Factory|View
     */
    public function edit(Table $table)
    {
        return view('tables.tableupdate', compact('table'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Table $table
     *
     * @return void
     */
    public function update(Request $request, Table $table)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param player $player
     * @param Table $table
     *
     * @return Factory|View
     */
    public function deletePlayer(Player $player, Table $table)
    {
        $player->tables()->detach();
        $player->state = 0;
        $player->save();
        return view('tables.tableupdate', compact('table'))
            ->with('warning','Player deleted from table successfully!');
    }

    /**
     * @param Table $table
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Table $table)
    {
        $table->players()->update(['state' => 0]);
        $table->delete();
        return back()->with('warning','Table deleted successfully!');
    }

    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function getGameTables(Game $game)
    {
        $tables = $game->tables()->get();
        return view('tables.tableList', compact('game', 'tables'));
    }
}
