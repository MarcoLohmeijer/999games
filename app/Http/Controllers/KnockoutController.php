<?php

namespace App\Http\Controllers;

use App\Game;
use App\Round;
use App\Player;
use App\GameRule;
use App\Table;
use App\table_players;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class KnockoutController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('knockout');
    }

    /**
     * @param Game $game
     * @param Round $round
     *
     * @return Factory|View
     */
    public function getKnockoutPlayers(Game $game, Round $round)
    {
        $gameRules = GameRule::where('game_id','=',$game->id)->get();
        $player = Player::where('state', '=', 1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->update(['state' => 0]);
        $players = Player::whereRaw('state=0')->where('checked_in','=',1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->orderBy('player_games.weight', 'desc')
            ->take($gameRules[0]->knockout_phase)
            ->inRandomOrder()->get();

        $cnt = count($players);
        $tables = Table::where('round_id','=',$round->id)->get();
        $cnt = count($tables);
        $spliced_table = $tables->splice($cnt/2);

        return view('knockout', compact('players', 'game', 'round', 'tables', 'spliced_table'));
    }

    /**
     * @param Request $request
     * @param Game $game
     * @param Round $round
     *
     * @return Factory|View|void
     */
    public function storeKnockoutPlayers(Request $request, Game $game, Round $round)
    {
        $gameRules = GameRule::where('game_id','=',$game->id)->get();

        table_players::where('round_id', '=', $round->id)->delete();
        Table::where('round_id', '=', $round->id)->delete();

        Player::where('state', '=', 1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->update(['state' => 0]);

        $players = Player::whereRaw('state=0')->where('checked_in','=',1)
            ->join('player_games', 'player_games.player_id', '=', 'players.id')
            ->where('player_games.game_id', '=', $game->id)
            ->orderBy('player_games.weight', 'desc')
            ->take($gameRules[0]->knockout_phase)
            ->inRandomOrder()->get();

        if(count($players) < $gameRules[0]->knockout_phase)
        {
            return back()->with('warning', 'There are not enough players');
        }

        $user_id = $request->user()->id;
        $players_cnt = count($players);

        // check: must be >= 4 players
        if ($players_cnt < 4) {
            return; // error
        }

        // check: players number must be power two
        for ($i = 1; $i > 0; $i = $i << 1) {
            if ($i > $players_cnt) {
                return; // error
            }
            if ($i == $players_cnt) {
                break;
            }
        }

        $table_cnt = $players_cnt / 2;

        $arr = [array_fill(0, $table_cnt, 0), array_fill(0, $table_cnt, 0)];
        $i = 0;
        while ($i < $players_cnt / 8) {
            $k = $players_cnt / 4 - $i;
            $n = $i * 4;

            $arr[0][$i * 2 + 0] = $n + 0;
            $arr[0][$i * 2 + 1] = $players_cnt - $n - 1;
            $arr[1][$k * 2 - 1] = $n + 1;
            $arr[1][$k * 2 - 2] = $players_cnt - $n - 2;

            if ($players_cnt == 4) {
                break;
            }

            $arr[0][$k * 2 - 1] = $n + 2;
            $arr[0][$k * 2 - 2] = $players_cnt - $n - 3;
            $arr[1][$i * 2 + 0] = $n + 3;
            $arr[1][$i * 2 + 1] = $players_cnt - $n - 4;

            $i++;
        }

        for ($i = 0; $i < $table_cnt; $i++) {
            $table = new Table;
            $table->number = $i + 1;
            $table->game_id = $game->id;
            $table->user_id = $user_id;
            $table->round_id = $round->id;
            $table->save();

            $n = intdiv($i, $table_cnt / 2);
            $m = $i % ($table_cnt / 2);

            for ($j = 0; $j < 2; $j++) {
                $p = $arr[$n][$m * 2 + $j];
                $player = $players[$p];

                $tp = new table_players;
                $tp->player_id = $player->id;
                $tp->table_id = $table->id;
                $tp->round_id = $round->id;
                $tp->save();

                $player->state = 1;
                $player->save();
            }
        }

        $left_players = [];

        $players->toArray();

        for($i = 0; $i < count($arr[0]); $i++)
        {
            $plr = $players[$arr[0][$i]];
            array_push($left_players, $plr);
        }

        $left_players;


        $tables = Table::where('round_id','=',$round->id)->get();
        $cnt = count($tables);
        $spliced_table = $tables->splice($cnt/2);

        return view('knockout', compact('players', 'game', 'round', 'tables', 'spliced_table'));
    }
}



