<?php

namespace App\Http\Controllers;

use App\Events\OrderShiped;
use App\Game;
use App\GameRule;
use App\Http\Requests\CreatePlayerRequest;
use App\player;
use App\Score;
use App\Round;
use http\Client\Curl\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;
use Illuminate\View\View;


class PlayerController extends Controller
{
    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function index(Game $game)
    {
        $players = $game->players()->Paginate(20);
        $rounds = Round::where('game_id', '=', $game->id)->get();
        $scores = Score::all();
        return view('player.playersOverview', compact('game', 'players', 'rounds', 'scores'));
    }

    /**
     * @param Request $request
     * @param Game $game
     * @param player $player
     *
     * @return Factory|View
     */
    public function login(Request $request, Game $game, player $player)
    {

        $players = Player::all();
        foreach ($players as $player){
            if ($player->code == $request->Code && $player->email == $request->email) {
                $player->checked_in = 1;
                $player->save();

                return view('/confirmation', compact('players'))->with('msg','Player checked in successfully!');
            }
        }
        return view('/confirmation', compact('players'))->with('msg','Player check in failed!');
    }

    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function create(Game $game)
    {
        return view('player.createPlayer', compact('game'));
    }

    /**
     * @param Request $request
     * @param Game $game
     */
    public function store(Request $request, Game $game)
    {
        //
    }

    /**
     * @param CreatePlayerRequest $request
     * @param Game $game
     *
     * @return Factory|View
     */
    public function storePlayer(CreatePlayerRequest $request, Game $game)
    {
        $gameRules = GameRule::where('id', $game->id)->first();
        if (count($game->players()->get()) < $gameRules['max_players']) {
            $player = new Player;
            $player->first_name = $request->get('first_name');
            $player->last_name = $request->get('last_name');
            $player->code = Str::random(6);
            $player->email = $request->get('email');
            $player->phonenumber = $request->get('phonenumber');
            $player->checked_in = false;
            $player->state = false;
            $player->user_id = 1;
            $player->save();
            $player->games()->attach($game->id);

            event(new OrderShiped($player));

            return view('/confirmation')->with('msg','Player added successfully!');

        }
        return view('/confirmation')->with('msg','Player limit reached!');

    }

    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function showPlayers(Game $game)
    {
        $players = $game->players()->get();
        return view('game.gameOverview', compact('players'));
    }


    /**
     * @param player $player
     *
     * @return Factory|View
     */
    public function show(player $player)
    {
        $scores = $player->scores()->get();
        return view('player.playerOverview', compact('scores', 'player'));
    }

    /**
     * @param Game $game
     * @param player $player
     *
     * @return Factory|View
     */
    public function edit(Game $game, player $player)
    {
        return view('player.playerupdate', compact('player', 'game'));
    }

    /**
     * @param Request $request
     * @param Game $game
     * @param player $player
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Game $game, player $player)
    {
        $player->first_name = $request->get('first_name');
        $player->last_name = $request->get('last_name');
        $player->email = $request->get('email');
        $player->phonenumber = $request->get('phonenumber');
        $player->save();
        $players = $game->players()->get();

        return redirect('/game/'.$game->id.'/player')->with(compact('players', 'player', 'game'))
            ->with('info','Player updated successfully!');
    }

    /**
     * @param Game $game
     * @param player $player
     *
     * @return Factory|View
     * @throws \Exception
     */
    public function destroy(Game $game, player $player)
    {
        $player->delete();
        $players = $game->players()->get();
        return view('player.playersOverview', compact('players', 'game'))->with('warning','Player deleted successfully!');
    }

    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function selectGame(Game $game)
    {
        $games = Game::all();
        return view('player.gamePlayer', compact('games'));
    }
}
