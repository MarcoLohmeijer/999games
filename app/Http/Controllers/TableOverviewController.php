<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TableOverviewController extends Controller
{
    /**
     * @return Factory|View
     */
    public function showTableOverview()
    {
        return view('tableOverview');
    }
}
