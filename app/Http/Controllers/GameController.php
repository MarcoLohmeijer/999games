<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Requests\UpdateGameRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class GameController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $games = Game::all();
        return view('game.game', compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return RedirectResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $game = new Game;
        $game->name = $request->get('name');
        $game->description = $request->get('description');
        $game->user_id = $request->user()->id;
        $game->save();
        return redirect()->route('game.gamerules.create', ['game' => $game->id])->with('success','Game created successfully!');
    }

    /**
     * Display the specified resource.
     * @param Game $game
     *
     * @return Factory|View
     */
    public function show(Game $game)
    {
        $game = Game::all()->where('id', $game);
        return view('game', ['game'=>$game]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Game $game
     *
     * @return Factory|View
     */
    public function edit(Game $game)
    {
        return view('game.edit', ['game'=>$game]);
    }

    /**
     * @param UpdateGameRequest $request
     * @param Game $game
     *
     * @return RedirectResponse
     */
    public function update(UpdateGameRequest $request, Game $game)
    {
        $game->name = $request->get('name');
        $game->description = $request->get('description');
        $game->save();

        $games = Game::all();
        return back()->with('info','Game updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     * @param Game $game
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    public function destroy(Game $game)
    {
        $game->delete();

        return back()->with('warning','Game deleted successfully!');
    }

    /**
     * @param Game $game
     * @return Factory|View
     */
    public function overview(Game $game)
    {
        return view('gameOverview', compact('game'));
    }
}
