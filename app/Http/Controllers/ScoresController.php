<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Requests\FindPlayerScoreRequest;
use App\Round;
use App\Player_games;
use App\Score;
use App\Player;
use App\Table;
use App\table_players;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class ScoresController extends Controller
{
    /**
     * @param $id
     *
     * @return Factory|View
     */
    public function index($id)
    {
        $player = Player::where('id', '=', $id)->paginate(20);
        return view('scores.scores', compact('player',));
    }

    /**
     * Create function
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * @param Game $game
     * @return Factory|View
     */
      public function scoresOverview(Game $game)
    {
        $n = 1;
        $players = Player::all();
          $playerGames = Player_games::where('game_id', $game->id)->join("players", 'player_games.player_id', '=', 'players.id')->get();
          $playerGames = $playerGames->sortByDesc('leaderboard_points')->sortByDesc("weight");
                foreach ($playerGames as $pg) {
                    Player::where('id', $pg->player_id)->update(['position' => $n ]);
                    $n++;
                }
        return view('scores.scoresOverview',compact('playerGames'));
    }


    /**
     * @param Request $request
     * @param Game $game
     * @param Round $round
     *
     * @return RedirectResponse|void
     */
    public function storeScore(Request $request, Game $game, Round $round)
    {
        $player_id = $request->input('player_id');

        $table = Table::where('round_id', '=', $round->id)
            ->whereExists(function ($query) use ($player_id) {
                $query->select('table_players.id')
                    ->from('table_players')
                    ->whereRaw('table_players.table_id=tables.id')
                    ->where('table_players.player_id', '=', $player_id);
            })
            ->first();

        if (!$table) {
            return;
        }

        Score::updateOrCreate([
            'player_id' => $player_id,
            'round_id' => $round->id
        ], [
            'user_id' => $request->user()->id,
            'points' => $request->get('points')
        ]);

        $tables = Table::where('round_id', '=', $round->id)->get();
        foreach ($tables as $table) {
            $players = $table->players()
                ->join('scores', 'scores.player_id', '=', 'players.id')
                ->where('scores.round_id', '=', $round->id)
                ->orderBy('scores.points', 'desc')
                ->get();

            if(count($table->players()->get())==4) {
                $arrPoints = [13, 9, 5, 1];

            }
            if(count($table->players()->get())==3) {
                $arrPoints = [13, 7, 1];

            }

            foreach ($players as $player) {
                $sc = Score::updateOrCreate([
                    'player_id' => $player->id,
                    'round_id' => $round->id
                ], [
                    'leaderboard_points' => (float)$arrPoints[0]
                ]);
                $sc['leaderboard_points'] = (float)$arrPoints[0];
                $sc->save();
                array_shift($arrPoints);
            }

            $players = $table->players()->get();
            $scores = Score::where('round_id', '=', $round->id)->get();

            $total = 0;
            foreach ($players as &$player) {
                $score = $scores->firstWhere('player_id', $player->id);
                $points = $score ? $score->points : 0;
                $total += $points;
            }

            foreach ($players as &$player) {
                $score = $scores->firstWhere('player_id', $player->id);
                $points = $score ? $score->points : 0;
                $weight = $score ? $score->weight : 0;
                $new_weight = $total > 0 ? $points * 100 / $total : 0;

                $player_points = Score::where('player_id', '=', $player->id)->get();
                $ldr_points = 0;
                foreach ($player_points as $player_point) {
                    $ldr_points += $player_point->leaderboard_points;
                }

                if ($new_weight != $weight) {
                    Score::updateOrCreate([
                        'player_id' => $player->id,
                        'round_id' => $round->id
                    ], [
                        'user_id' => $request->user()->id,
                        'weight' => $new_weight,
                    ]);

                    $total_weight = Score::where('player_id', '=', $player->id)
                        ->whereIn('round_id', function ($query) use ($game) {
                            $query->select('round_id')
                                ->from('rounds')
                                ->where('game_id', '=', $game->id);
                        })
                        ->avg('weight');

                    Player_games::where([
                        'player_id' => $player->id,
                        'game_id' => $game->id
                    ])->update([
                        'weight' => $total_weight,
                        'leaderboard_points' => $ldr_points
                    ]);
                }
            }
        }
        $tables = Table::where('round_id','=',$round->id)->get();
        $cnt = count($tables);
        $spliced_table = $tables->splice($cnt/2);
        return back()->with(compact('tables',  'spliced_table'))->with('success','Player score added successfully!');
    }
    /**
     * @param Score $score
     */
    public function show(Score $score)
    {
       //
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function showGameScore($id)
    {
        $scores = Score::where('round_id', $id)
            ->join('players', 'players.id', '=', 'scores.player_id')
            ->get();
        return view('scores.scores', compact('scores') );
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function showPlayerScore($id)
    {
        $player = Player::find($id);
        return view('scores.scores', compact('player'));
    }

    /**
     * @param Score $score
     *
     * @return Factory|View
     */
    public function edit(Score $score)
    {
        return view('scores.scoresupdate', compact('score'));
    }

    /**
     * @param Request $request
     * @param Score $score
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Score $score)
    {
        $score->points = $request->get('points');
        $score->weight = $request->get('weight');
        $score->save();
        //also update the weight in player_games for the max round
        $scores = Score::all();
        return redirect('player/'.$score->player_id)->with(compact('scores'))
            ->with('info','Player score updated successfully!');
    }

    /**
     * @param Score $score
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function destroy(Score $score)
    {
        $score->delete();
        $scores = Score::all();
        return redirect('player/'.$score->player_id)->with(compact('scores'))
            ->with('warning','Score deleted successfully!');
    }

    /**
     * @param FindPlayerScoreRequest $request
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function findPlayerScore(FindPlayerScoreRequest $request)
    {
        if ($player = Player::where('code', '=', $request->get('code'))->first()) {
            $scores = $player->scores()->get();
            $tables = $player->tables()->orderBy('created_at', 'desc')->get();

            return view('scores.playerScoreOverview', compact('player', 'scores', 'tables'));
        }

        return redirect('/login')->with('error','Code is not in use!');
    }
}
