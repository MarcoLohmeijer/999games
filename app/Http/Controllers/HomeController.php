<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Game;
use App\Player;
use Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $games = Game::all();

        return view('home', compact('games'));
    }

    /**
     * @return Factory|View
     */
    public function adminLogin()
    {
        return view('auth.loginAdmin');
    }

}
