<?php

namespace App\Http\Controllers;

use App\Round;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ClockController extends Controller
{
    /**
     * @param $round_id
     * @return Factory|View
     */
    public function showClock($round_id)
    {
        $round = Round::find($round_id);

        return view("countdownClock", compact('round'));
    }
}
