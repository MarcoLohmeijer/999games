<?php


namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController
{
    /**
     * @return Factory|View
     */
    public function showAdminOverview()
    {
        return view('adminOverview');
    }

    /**
     * @return Factory|View
     */
    public function showAdminTafelsOverview()
    {
        return view('adminTafelsOverview');
    }

    /**
     * @return Factory|View
     */
    public function showAdminTafelToevoegen()
    {
        return view('adminTafelToevoegen');
    }

    /**
     * @return Factory|View
     */
    public function showAdminSpelersOverview()
    {
        return view('adminSpelersOverview');
    }
}
