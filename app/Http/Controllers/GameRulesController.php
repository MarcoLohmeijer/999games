<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGameRuleRequest;
use App\Game;
use App\GameRule;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class GameRulesController extends Controller
{
    /**
     * @param Game $game
     *
     * @return Factory|View
     */
    public function index(Game $game)
    {
       $gamerules = $game->gameRules()->first();

        return view('gamerules.index', compact('gamerules', 'game'));
    }

    /**
     * @param Game $game
     * @return Factory|View
     */
    public function create(Game $game)
    {
        return view('gamerules.create', compact('game'));
    }

    /**
     * @param Game $game
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Game $game, Request $request)
    {
        $gamerule = new GameRule();
        $gamerule->max_players = $request->get('max_players');
        $gamerule->min_players = $request->get('min_players');
        $gamerule->max_rounds = $request->get('max_rounds');
        $gamerule->knockout_phase = $request->get('knockout_phase');
        $gamerule->user_id = $request->user()->id;
        $gamerule->game_id = $game->id;

        $gamerule->save();

        return redirect()->route('game.overview', ['game' => $game->id])->with('success','Gamerule created successfully!');
    }

    /**
     * @param Game $game
     * @param GameRule $gameRule
     *
     * @return Factory|View
     */
    public function show(Game $game, GameRule $gameRule)
    {
        return view('gamerules.show', compact('game', 'gameRule'));
    }

    /**
     * @param Game $game
     * @param $gamerule_id
     *
     * @return Factory|View
     */
    public function edit(Game $game, $gamerule_id)
    {
        $gameRule = GameRule::find($gamerule_id);
        return view('gamerules.edit', compact('game', 'gameRule'));
    }

    /**
     * @param Request $request
     * @param Game $game
     * @param GameRule $gamerule
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Game $game, GameRule $gamerule)
    {
        $gamerule->max_players = $request->get('max_players');
        $gamerule->min_players = $request->get('min_players');
        $gamerule->max_rounds = $request->get('max_rounds');
        $gamerule->knockout_phase = $request->get('knockout_phase');
        $gamerule->user_id = $request->user()->id;
        $gamerule->game_id = $game->id;

        $gamerule->save();

        return redirect()->route('game.overview', ['game' => $game->id])->with('info','Gamerule updated successfully!');
    }
}

