<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateGameRuleRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'max_players' => 'required|int',
            'min_players' => 'required|int',
            'max_rounds' => 'required|int',
            'knockout' => 'required|int',
            'game'=>'required|string',
        ];
    }
}
