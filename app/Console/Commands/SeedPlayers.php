<?php

namespace App\Console\Commands;

use App\Player;
use Illuminate\Console\Command;

class SeedPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:players {value=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed random players into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = intval($this->argument('value'));
        factory(Player::class, $amount)->create();
        $this->line('successfully seeded ' . $amount . ' player(s)');
    }
}
