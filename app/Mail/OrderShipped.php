<?php

namespace App\Mail;

use App\User;
use App\Player;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /** @var Player */
    private $player;

    /**
     * Create a new message instance.
     *
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @return OrderShipped
     */
    public function build()
    {
        $code = $this->player->code;

        return $this->from('no-reply@999games.nl')
            ->to($this->player->email)
            ->subject('Registratie')
            ->view('emails.email', compact('code'));
    }
}
