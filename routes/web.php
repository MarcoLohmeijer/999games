<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\GameRulesController;
use App\Http\Controllers\ScoresController;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/players/{game}', 'PlayerController@showPlayers')->name('showPlayers');

//home
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

//clock
Route::get('/clock', 'ClockController@showClock')->middleware('auth');
Route::get('/clock/{round_id}', 'ClockController@showClock')->name('showClock')->middleware('auth');


//knockout
//Route::resource('knockout', 'KnockoutController');
Route::get('/game/{game}/knockout/{round}', 'KnockoutController@getKnockoutPlayers')->name('getKnockoutPlayers')->middleware('auth');
//Route::get('/game/{game}/knockout/{round}', 'KnockoutController@getKnockoutPlayers')->name('getKnockoutPlayers')->middleware('auth');
Route::post('/game/{game}/knockout/{round}', 'KnockoutController@storeKnockoutPlayers')->name('storeKnockoutPlayers')->middleware('auth');

//players
Route::resource('game.player', 'PlayerController');
Route::get('player.loginPlayer', 'PlayerController@login')->name('game.player.login');
Route::get('player.gamePlayer', 'PlayerController@selectGame')->name('selectGame');

//rounds
Route::resource('rounds', 'RoundsController')->middleware('auth');
Route::get('/store_round', 'RoundsController@storeRoundForm')->middleware('auth');
route::get('getOneRound/{round}', 'RoundsController@getOneRound')->middleware('auth');
Route::get('/game/{game}/rounds', 'RoundsController@showRounds')->name('showRounds')->middleware('auth');
Route::post('/game/{game}/round', 'RoundsController@storeRound')->name('storeRound')->middleware('auth');
Route::post('/game/{game}/knockout', 'RoundsController@storeKnockoutRound')->name('storeKnockoutRound')->middleware('auth');

//games
Route::resource('game', 'GameController')->middleware('auth');
Route::get('/gameoverview', 'HomeController@index')->middleware('auth');
Route::get('/game/{game}/overview', 'GameController@overview')->name('game.overview')->middleware('auth');

//tables
Route::resource('tables', 'TablesController')->middleware('auth');
Route::get('tables/{table}/player/{player}', 'TablesController@deletePlayer')->name('deletePlayer')->middleware('auth');
Route::get('/admin_tafels_overview', 'AdminController@showAdminTafelsOverview')->middleware('auth');
Route::get('/admin_tafel_toevoegen', 'AdminController@showAdminTafelToevoegen')->middleware('auth');
Route::get('game/{game}/round/{round}/tables', 'TablesController@showTables')->name('showTables')->middleware('auth');
Route::post('/game/{game}/round/{round}/tables', 'TablesController@storeTable')->name('storeTable')->middleware('auth');
Route::get('/game/{game}/tables', 'TablesController@getGameTables')->name('game.tables')->middleware('auth');
Route::post('/game/{game}/table', 'TablesController@store')->name('game.table.store')->middleware('auth');

//scores
Route::resource('scores', 'ScoresController')->middleware('auth');
Route::get('gameScores/{id}', 'ScoresController@showGameScore')->middleware('auth');
Route::get('score/{id}', 'ScoresController@showPlayerScore')->name('showPlayerScore')->middleware('auth');
Route::get('game/{game}/score', 'ScoresController@scoresOverview')->name('scoresOverview')->middleware('auth');
Route::post('/game/{game}/round/{round}/score', 'ScoresController@storeScore')->name('storeScore')->middleware('auth');
Route::post('/player/{game}', 'PlayerController@storePlayer')->name('storePlayer');


//player home


//player scores
Route::get('/player_score', 'ScoresController@showFindPlayerScore')->name('showFindPLayerScore');
Route::post('/player_score', 'ScoresController@findPlayerScore')->name('findPlayerScore');

//GameRules
Route::resource('game.gamerules', 'GameRulesController')->middleware('auth');
Route::get('/gamerule_delete/{gamerule}', 'GameRulesController@destroy')->name('deleteGameRules')->middleware('auth');

//Admin
Route::get('/adminlogin', 'HomeController@adminLogin')->name('adminLogin');

Route::post('/playerLogin', 'PlayerController@login')->name('playerLogin');
