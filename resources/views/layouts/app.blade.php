<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@extends('layouts.head')
<div>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-danger shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{URL::asset('/images/logo.jpg')}}" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a href="{{route('login')}}" class="btn btn-danger float-left"
                                   role="button">Player Log In / Sign Up</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a href="{{route('adminLogin')}}" class="btn btn-danger float-left"
                                       role="button">Crew Log In</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div id="page-container">
        <div id="content-wrap">
            <main class="py-4">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">@include('messages')</div>
                    <div class="col-sm-2"></div>
                </div>
                @yield('content')
            </main>
        </div>
    </div>
    <div class="container-fluid bg-danger text-white">
        <footer id="footer" class="page-footer font-small pt-4">
            <div class="container text-center text-md-left">
                <div class="row text-center text-md-left mt-3 pb-3">
                    <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold">999games</h6>
                        <p>
                            999 Games bestaat al meer dan 25 jaar, dus er is inmiddels een boel spelgeschiedenis
                            geschreven.
                            Bekijk de ontwikkeling van ons bedrijf aan de hand van de
                            <a href="https://www.999games.nl/over-ons">
                                chronologische tijdlijn
                            </a>
                        </p>
                    </div>
                    <hr class="w-100 clearfix d-md-none">
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold">Meer van 999games</h6>
                        <p>
                            <a href="https://www.999games.nl/spellen.html">Producten</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/nieuws">Nieuws</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/pers-informatie">Pers</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/online-try-out-games">Online try-out games</a>
                        </p>
                    </div>
                    <hr class="w-100 clearfix d-md-none">
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold">Bestel informatie</h6>
                        <p>
                            <a href="https://www.999games.nl/klantenservice#bestellen">Bestellen</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/klantenservice#betalen">Betalen</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/klantenservice#bezorgen">Bezorgen</a>
                        </p>
                        <p>
                            <a href="https://www.999games.nl/storelocator">Spellenwinkel bij jou in de buurt</a>
                        </p>
                    </div>
                    <hr class="w-100 clearfix d-md-none">
                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                        <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                        <p>
                            <i class="fas fa-home"></i>Postbus 60230 <br>1320 AG Almere, Nederland</p>
                        <p>
                            <i class="fas fa-envelope"></i> info@gmail.com</p>
                        <p>
                            <i class="fas fa-phone"></i>KvK 32088639 <br>BTW NL 8103.93.372.B01</p>
                        <p>
                            <i class="fas fa-print"></i>0900 - 999 0000</p>
                    </div>
                </div>
                <hr>
                <div class="row d-flex align-items-center">
                    <div class="col-md-7 col-lg-8">
                        <p class="text-center text-md-left">© Copyright:
                            <a href="https://www.999games.nl/">
                                <strong> 999games</strong>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
</html>
