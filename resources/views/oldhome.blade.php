@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row h-100">
                    @foreach($games as $game)
                        <div class="col-sm-5">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">participate</button>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Voer uw gegevens in</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <form method="post" action="{{route('storePlayer', [$game->id])}}">
                                            @csrf
                                            <div class="modal-body">
                                                <input name="first_name" type="text" class="form-control mt-2" placeholder="First name"
                                                       required="required"/>
                                                <input name="last_name" type="text" class="form-control mt-2" placeholder="Last name"
                                                       required="required"/>
                                                <input name="email" type="email" class="form-control mt-2" placeholder="Email"
                                                       required="required"/>
                                                <input name="phonenumber" type="number" class="form-control mt-2" placeholder="Phonenumber"
                                                       required="required"/>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn-warning" value="confirm">
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            {{$game->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection


