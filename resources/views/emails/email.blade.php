<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registratie</title>
</head>
<body>
<div>
    <h3>Bedankt voor uw inschrijving.</h3>

    <p>Hierbij uw registratie code: {{$code}}</p>

    <p>Volg deze link <a href="{{URL::to('/login')}}">link</a>.</p>
    <p>Op de dag van het toernooi om jezelf te melden dat je er bent met de registratie code.</p>
</div>
</body>
</html>
