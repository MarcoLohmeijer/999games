@extends('layouts.app')

@section('content')
    <div class="row h-100">
    <div class="col-3"></div>
    <div class="col-6 m-10">
            <h1>Player Scores</h1><hr>
        <div class="row">
                <div class="col-sm-2"><h5>Position</h5></div>
                <div class="col-sm-4"><h5>Name</h5></div>
                <div class="col-sm-3"><h5>Points</h5></div>
                <div class="col-sm-3"><h5>Weight</h5></div>
            @foreach($playerGames as $playerGame)
                <div class="col-sm-12"><hr></div>
                <div class="col-sm-2">{{$playerGame->position}}</div>
                <div class="col-sm-4">{{$playerGame->first_name}}</div>
                <div class="col-sm-3">{{$playerGame->leaderboard_points}}</div>
                <div class="col-sm-3">{{$playerGame->weight}}</div>
            @endforeach
        </div>

        <hr>
        <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
    </div>
    <div class="col-3"></div>
    </div>
@endsection
