@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h4>uitslag</h4>

                 <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Naam</th>
                            <th scope="col"></th>
                            <th scope="col">Punten</th>
                            <th scope="col">Gewicht</th>
                        <tbody>
                    @foreach($scores as $score)
                        <tr>
                            <td> {{$score['first_name']}}</td>
                            <td> {{ $score['last_name']}}</td>
                            <td>{{$score['points']}}</td>
                            <td>{{$score['weight']}}</td>
                        </tr>
                    @endforeach
                     </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
