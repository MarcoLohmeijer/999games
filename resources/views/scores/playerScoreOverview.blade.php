@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-2"></div>
        <div class="col-sm-1">
            <form class="float-left position-fixed" action="{{ route('login') }}">
                <button class="btn">
                    <i class="fas fa-angle-left"></i> Back
                </button>
            </form>
        </div>
        <div class="col-6 m-10">
            <h3>Scores {{$player->first_name}}</h3>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Points</th>
                    <th scope="col">Weight</th>
                </tr>
                </thead>
                <tbody>
                @foreach($scores as $score)
                    <tr>
                        <td>{{$score->leaderboard_points}}</td>
                        <td>{{$score->weight}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-6 m-10">
            <h3>Tables</h3>
            @foreach($tables as $table)
                <tr>
                    <td>{{$table->number}}</td>
                </tr>
{{--                <p>Other players on your table are:</p>--}}
{{--                <tr>--}}
{{--                @foreach($table->players() as $player())--}}
{{--                        <td>{{$player->first_name}}</td>--}}
{{--                    @endforeach--}}
{{--                </tr>--}}
            @endforeach
        </div>
        <div class="col-3"></div>
    </div>
@endsection
