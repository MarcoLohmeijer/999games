@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row card mt-3">
                    <div class="card-header">
                        <h4>Invoer uitslag</h4>
                    </div>
                    <div class="card-body mt-3">
                        <form method="post" action="{{route('scores.update', [$score->id])}}" class="row form">
                            @csrf
                            @method('PUT')
                            <div class="form-group col-md-12">
                                <label for="user_code">points</label>
                                <input class="form-control" type="number" name="points" value="{{$score->points}}"
                                       required>
                            </div>
                            <div class="form-group col-md-6 mt-3">
                                <label for="table">weight</label>
                                <input class="form-control" type="number" name="weight" value="{{$score->weight}}"
                                       required>
                            </div>
                            <div class="form-group col-md-8 mt-3">
                                <input type="submit" class="btn btn-primary" value="Verzend">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
