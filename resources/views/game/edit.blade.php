@extends('layouts.app')

@section('content')
    <div class="modal-body">
        <form action="{{route('game.update', [$game->id])}}" method="post">
            @csrf
            {{method_field('PUT')}}
            <input type="string" name="name" value="{{$game->name}}">
            <input type="text" name="description" value="{{$game->description}}">
            <button class="btn btn-primary" type="submit" name="submit">submit</button>
        </form>
    </div>
    <a href="{{route('game.index')}}"> <button class="btn btn-primary" type="submit" name="submit">return</button></a>
@endsection
