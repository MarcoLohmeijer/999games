@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($players as $player)
            <div class="col-md-8">
                <a href="/player/{{$player->id}}">{{$player->first_name}} {{$player->last_name}}</a>
                <a href="{{route('showPlayerScore', ['id' => $player->id])}}">playersccore</a>
                <form method="get" action="{{route('showPlayerScore', ['player' => $player->id])}}">
                    @csrf
                    <input type="submit" class="btn-warning" value="Add score">
                </form>
            </div>
            @endforeach
        </div>
    </div>
@endsection
