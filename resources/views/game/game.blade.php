@extends ('layouts.app')

@section('content')
    @include('components.modalCreateGame')
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <h1>Overzicht spellen</h1>
            <button type="button" class="btn btn-secondary float-right m-3 shadow" data-toggle="modal"
                    data-target="#modalCreateGame">Voeg een spel toe
            </button>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="container border-left border-right border-top border-bottom rounded p5 bg-waring shadow">
                @foreach($games as $game)
                    <div class="card m-3 shadow-lg p-4 mb-4 bg-white">
                        <div class="card-header shadow">{{$game['name']}}</div>
                        <div class="card-body">
                            <p class="card-text">{{$game['description']}}</p>
                            <div class="btn-group float-left">
                                <form method="get" action="{{route('showRounds', [$game->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary shadow mr-3">rounds</button>
                                </form>
                                <form method="get" action="{{route('getKnockoutPlayers', [$game->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary shadow">knockout</button>
                                </form>
                                <form method="get" action="{{route('showPlayers', [$game->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary shadow">players</button>
                                </form>
                            </div>
                            <div class="btn-group float-right">
                                <form method="get" action="{{route('game.edit', [$game->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary shadow">pas aan</button>
                                </form>
                                <form method="post" action="{{route('game.destroy', [$game->id])}}">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger shadow">verwijder</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
@endsection
