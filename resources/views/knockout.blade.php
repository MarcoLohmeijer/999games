@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row h-100">
            <div class="col-sm-2"></div>
            <form method="post" action="{{route('storeKnockoutPlayers', [$game->id, $round->id])}}">
                @csrf
                <button type="submit" class="btn btn-danger mr-1">Post</button>
            </form>
            <div class="col-sm-4">
{{--                @for($i = 0; $i < count($arr[0]); $i++)--}}
                @foreach($tables as $table)
                    <table class="table">
                        Table: {{$table->number}}
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Score</th>
                        <th scope="col">Weight</th>
                        <th scope="col">Add Score</th>
                    </tr>
                    </thead>
                        <tbody>
                    @foreach($table->players()->get() as $player)
                            <tr>
                                <th scope="row"></th>
                                <td>{{$player['first_name']}}</td>
                    <td>{{$player['points']}}</td>
                    <td>{{number_format($player['weight'], 2)}}</td>
                    <div class="btn-group float-right">
                        <td>
                            <button type="button" class="btn-primary" data-toggle="modal"
                                    data-target="#myModal{{$player['id']}}">+
                            </button>
                        </td>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal{{$player['id']}}" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Voer de score in</h4>
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;
                                        </button>
                                    </div>
                                    <form method="post"
                                          action="{{route('storeScore', [$game->id, $round->id])}}">
                                        @csrf
                                        <input type="hidden" name="player_id" id="player_id"
                                               value="{{$player['id']}}">
                                        <div class="form-group col-md-12">
                                            <label for="user_code">points</label>
                                            <input class="form-control" type="number" name="points"
                                                   required>
                                        </div>
                                        <div class="form-group col-md-8 mt-3">
                                            <input type="submit" class="btn btn-primary"
                                                   value="Verzend">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                            </tr>
                    @endforeach
                        </tbody>
                    </table>
                @endforeach
{{--                    <div class="card sixteen-finals">--}}
{{--                        <div class="card-title player text-center">--}}
{{--                            <b>Table: {{$table->number}}</b><br>--}}
{{--                            <br>--}}
{{--                            <ul>--}}
{{--                                @foreach($table->players()->get() as $player)--}}
{{--                                    <li>{{$player['first_name']}}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--                @endfor--}}
                {{--            @foreach($players as $player)--}}
                {{--                @if()--}}
                {{--            @endforeach--}}
            </div>
            {{--        <div class="col-sm-1">--}}
            {{--            @for($i = 0; $i < 4; $i++)--}}
            {{--                <div class="card eighth-card-{{$i}}">--}}
            {{--                    <div class="card-title player text-center">--}}
            {{--                        <b>Table: 1</b><br>--}}
            {{--                        player<br>--}}
            {{--                        player--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endfor--}}
            {{--        </div>--}}
            {{--        <div class="col-sm-1">--}}
            {{--            @for($i = 0; $i < 2; $i++)--}}
            {{--                <div class="card quarter-finale-{{$i}}">--}}
            {{--                    <div class="card-title player text-center">--}}
            {{--                        <b>Table: 1</b><br>--}}
            {{--                        player<br>--}}
            {{--                        player--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endfor--}}
            {{--        </div>--}}
            {{--        <div class="col-sm-2">--}}
            {{--            @for($i = 0; $i < 1; $i++)--}}
            {{--                <div class="card finale">--}}
            {{--                    <div class="card-title player text-center">--}}
            {{--                        <b>Table: 1</b><br>--}}
            {{--                        player<br>--}}
            {{--                        player--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endfor--}}
            {{--        </div>--}}
            {{--        <div class="col-sm-1">--}}
            {{--            @for($i = 0; $i < 2; $i++)--}}
            {{--                <div class="card quarter-finale-{{$i}}">--}}
            {{--                    <div class="card-title player text-center">--}}
            {{--                        <b>Table: 1</b><br>--}}
            {{--                        player<br>--}}
            {{--                        player--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endfor--}}
            {{--        </div>--}}
            {{--        <div class="col-sm-1">--}}
            {{--            @for($i = 0; $i < 4; $i++)--}}
            {{--                <div class="card eighth-card-{{$i}}">--}}
            {{--                    <div class="card-title player text-center">--}}
            {{--                        <b>Table: 1</b><br>--}}
            {{--                        player<br>--}}
            {{--                        player--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endfor--}}
            {{--        </div>--}}
            <div class="col-sm-4">
                @foreach($spliced_table as $table)
                    <table class="table">
                        Table: {{$table->number}}
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Score</th>
                            <th scope="col">Weight</th>
                            <th scope="col">Add Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($table->players()->get() as $player)
                            <tr>
                                <th scope="row"></th>
                                <td>{{$player['first_name']}}</td>
                                <td>{{$player['points']}}</td>
                                <td>{{number_format($player['weight'], 2)}}</td>
                                <div class="btn-group float-right">
                                    <td>
                                        <button type="button" class="btn-primary" data-toggle="modal"
                                                data-target="#myModal{{$player['id']}}">+
                                        </button>
                                    </td>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal{{$player['id']}}" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Voer de score in</h4>
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;
                                                    </button>
                                                </div>
                                                <form method="post"
                                                      action="{{route('storeScore', [$game->id, $round->id])}}">
                                                    @csrf
                                                    <input type="hidden" name="player_id" id="player_id"
                                                           value="{{$player['id']}}">
                                                    <div class="form-group col-md-12">
                                                        <label for="user_code">points</label>
                                                        <input class="form-control" type="number" name="points"
                                                               required>
                                                    </div>
                                                    <div class="form-group col-md-8 mt-3">
                                                        <input type="submit" class="btn btn-primary"
                                                               value="Verzend">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endforeach
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
@endsection
