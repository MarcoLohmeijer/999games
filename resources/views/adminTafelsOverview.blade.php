@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Tafels overzicht</h1><hr>
            <div class="row h-100">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3">
                            Tafel ID
                        </div>
                        <div class="col-sm-7">
                            Tafel Nummer / Naam
                        </div>
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-12"></div>
                    </div><hr>

                    <div class="row">

                        <div class="col-sm-3">

                        </div>
                        <div class="col-sm-7">

                        </div>
                        <div class="col-sm-2">
                            <form method='post' action =''>
                                <input type='hidden' name='id'>
                                <button type ='action' class="btn btn-primary" name="delete">Verwijder</button>
                            </form>
                        </div>
                    </div>
                    <a href="admin_overview" class="btn btn-primary float-left" role="button">Terug</a>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
@endsection
