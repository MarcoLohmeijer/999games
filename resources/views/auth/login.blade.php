@extends('layouts.app')

@section('content')
    <div class="row h-100" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-2"></div>
        <div class="col-sm-4">
            <div class="row h-100">
                <div class="col-sm-12">
                    <h5>Player Check In!</h5><hr>
                    <form method="post" action="{{ route('playerLogin') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email"><h10>Player E-mail:</h10></label>
                            <input name="email" type="text" class="form-control" id="email" required>
                        </div>

                        <div class="form-group">
                            <label for="Code"><h10>Code:</h10></label>
                            <input name="Code" type="Code" class="form-control" id="Code" required>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            {{ __('Check In') }}
                        </button>
                    </form>
                </div>

                <div class="col-sm-12 m-1">
                    <h5>Player scores overview</h5><hr>
                    <form method="post" action="{{ route('findPlayerScore') }}">
                        @csrf
                        <div class="form-group">
                            <label for="code"><h10>Player code:</h10></label>
                            <input name="code" type="text" class="form-control" id="code" required>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>

            </div>
        </div>

        <div class="col-sm-5">
            <div class="row h-100">
                <div class="col-sm-12">
                    <h5>New Players Sign Up</h5><hr>
                    <h10>New players can sign up here!</h10><br><br>
                    <a href="{{route('selectGame')}}" class="btn btn-primary float-left" role="button">Sign Up</a>
                </div>
            </div>
        </div>
        <div class="col-sm-1">

        </div>
    </div>
    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-9">
            <img src="{{URL::asset('/images/pic1.jpg')}}" alt="logo">
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
