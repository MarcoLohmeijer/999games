@extends('layouts.app')

        @section('content')
            <div class="row h-100">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <h1>Tafel toevoegen</h1><hr>
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <h10>Voeg nieuwe tafel toe</h10><hr>
                            <form method='post' action='' enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="name">Tafel Nummer / Naam:</label>
                                    <input type="text" class="form-control" name="name" id="name" required="required"/>
                                </div>
                                <a href="admin_overview" class="btn btn-primary float-left" role="button">Terug</a>
                                <button type="submit" class="btn btn-primary float-right">Voeg toe</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
@endsection
