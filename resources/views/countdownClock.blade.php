
@extends('layouts.app')
@section('content')
    <div style="height: 100vh">
        <div class="mt-auto mb-auto">
            <div class="text-dark" id="text-before" style="font-size: 175px" ></div>
            <p class="text-center"><div class="text-primary" id="clock" style="font-size:350px; text-shadow: 5px 3px darkgrey"></div></p>
        </div>
    </div>
@endsection
