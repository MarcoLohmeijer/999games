@extends('layouts.app')

@section('content')
    @include('components/modalCreateGame')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8">
                <h1 class="text-center">@lang('text.home.title')</h1>
                <hr>
                <div class="row h-100">
                    @foreach($games as $game)
                        <div class="col-md-4">
                            <div class="card w-100">
                                <div class="card-body">
                                    <h3>{{$game->name}}</h3>
                                    <hr>
                                    <p class="card-text">{{$game->description}}</p>
                                    <a href="{{route('game.overview', ['game' => $game->id])}}" class="btn btn-primary">Go to game overview</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-2">
                <button type="button" class="btn btn-primary text-light" data-toggle="modal" data-target="#modalCreateGame">@lang('buttons.createNewGame')</button>
            </div>
        </div>
    </div>
@endsection


