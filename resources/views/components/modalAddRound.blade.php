<!-- The Modal -->
<div class="modal" id="modalAddRound">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Add Round</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('storeRound', [$game->id])}}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Round:</label>
                        <input type="text" name="name" class="form-control" placeholder="Round" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="time">Time:</label>
                        <input type="time" name="time" class="form-control" placeholder="Enter start time" id="time" required>
                    </div>
                    <div class="form-group">
                        <label for="start_time">Start time:</label>
                        <input type="time" name="start_time" class="form-control" placeholder="Enter start time" id="start_time" required>
                    </div>
                    <button type="submit" class="btn btn-secondary m-3 shadow">Submit</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
