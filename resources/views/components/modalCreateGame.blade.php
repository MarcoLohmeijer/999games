<!-- The Modal -->
<div class="modal" id="modalCreateGame">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">@lang('text.home.addNewGame')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('game.store')}}">
                    @csrf
                    <div class="form-group">
                        <label for="name">@lang('text.formLabel.name')</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="description">@lang('text.formLabel.description')</label>
                        <input type="text" name="description" class="form-control" placeholder="Enter description" id="description">
                    </div>
                    <button type="submit" class="btn btn-secondary m-3 shadow">@lang('buttons.create')</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('buttons.close')</button>
            </div>

        </div>
    </div>
</div>
