@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1>Game Rule</h1><hr>
            <div class="row h-100">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="card">
                            <div class="card-header">
                                Game Rule ID:
                                {{ $gamerule->id }}
                            </div>
                            <div class="card-body">
                                Game Name:
                                {{ $gamerule->game['name']}}<br>
                                Max Players:
                                {{$gamerule['max_players']}}<br>
                                Min Players:
                                {{$gamerule['min_players']}}<br>
                                Max Rounds:
                                {{$gamerule['max_rounds']}}<br>
                                Amount of player for the Knockout Phase:
                                {{$gamerule['knockout_phase']}}<br>
                                User ID:
                                {{$gamerule['user_id']}}<br>
                                Created:
                                {{$gamerule['created_at']}}<br>
                                Updated:
                                {{$gamerule['updated_at']}}<br>
                            </div>
                        </div>
                    </div><br>
                    <a href="{{route('gamerules.index')}}" class="btn btn-primary float-left" role="button">Back</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4"></div>
    </div>
@endsection
