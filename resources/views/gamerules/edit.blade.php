@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-9">
            <h1>Game Rule Edit</h1><hr>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="card w-100">
                        <div class="card-body">
                            <form method='post' action="{{route('game.gamerules.update', ['game' => $game->id, 'gamerule' => $gameRule->id])}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="form-group1">
                                    <label for="name"><h10>Max Players:</h10></label>
                                    <input type="number" class="form-control" name="max_players" id="name" required="required"
                                           value="{{$gameRule->max_players}}"/>
                                </div><br>

                                <div class="form-group1">
                                    <label for="name"><h10>Min PLayers:</h10></label>
                                    <input type="number" class="form-control" name="min_players" id="name" required="required"
                                           value="{{$gameRule->min_players}}"/>
                                </div><br>

                                <div class="form-group1">
                                    <label for="name"><h10>Max Rounds:</h10></label>
                                    <input type="number" class="form-control" name="max_rounds" id="name" required="required"
                                           value="{{$gameRule->max_rounds}}"/>
                                </div><br>

                                <div class="form-group1">
                                    <label for="name"><h10>Amount of player for the Knockout Phase:</h10></label>
                                    <input type="number" class="form-control" name="knockout_phase" id="name" required="required"
                                           value="{{$gameRule->knockout_phase}}"/>
                                </div><br>

                                <button type="submit" class="btn btn-primary float-right">Update</button>
                            </form>
                                <a href="{{route('game.overview', ['game' => $game->id])}}" class="btn btn-primary float-left" role="button">Back</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
    </div>
@endsection
