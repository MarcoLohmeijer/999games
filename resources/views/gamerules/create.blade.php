@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1>Add New Game Rules</h1><hr>
            <div class="row h-100">
                <div class="col-sm-8">
                    <form method='post' action={{route('game.gamerules.store', ['game' => $game->id])}} enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Max Players:</label>
                            <input name="max_players" type="number" class="form-control" id="name" required="required"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Min Players:</label>
                            <input name="min_players" type="number" class="form-control" id="name" required="required"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Amount of player for the Knockout Phase:</label>
                            <input name="knockout_phase" type="number" class="form-control" id="name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="name">Max Rounds:</label>
                            <input name="max_rounds" type="number" class="form-control" id="name" required="required"/>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary float-right">Add</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
@endsection
