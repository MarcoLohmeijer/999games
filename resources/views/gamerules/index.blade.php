@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1>Game Rules<a href="{{route('game.gamerules.create', ['game'=> $game->id])}}" class="btn btn-primary float-right" role="button">Add New Game Rules</a></h1><hr>
            <div class="row h-100">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            {{$gamerules['id']}}

                            {{ $gamerules->game['name']}}

                            {{$gamerules['max_players']}}

                            {{$gamerules['min_players']}}

                            <form method='get' action="{{route('game.gamerules.edit', ['game' => $game->id, 'gamerule' => $gamerules->id])}}"
                                  style="display: inline-block; float: right">
                                @csrf
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
@endsection
