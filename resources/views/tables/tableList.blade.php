@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                @foreach($tables as $table)
                    <div class="col-2">
                        <div class="card m-3 shadow-lg p-4 bg-white">
                            <div class="card-header">
                                <div class="float-left">
                                    Table: {{$table->number}}
                                </div>
                            </div>
                            <div class="card-body">

                                <div class="btn-group float-right">
                                    <form method="post" action="{{route('tables.destroy', [$table->id])}}">
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="btn btn-danger mr-1">delete</button>
                                    </form>
                                    <form method="get" action="{{route('tables.edit', [$table->id])}}">
                                        @csrf
                                        <button type="submit" class="btn btn-primary">edit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>

            <div class="col-sm-1">
                <form method="post" action="{{route('game.table.store', ['game' => $game->id])}}">
                    @csrf
                    <button type="submit" class="btn btn-primary mr-1">Add new table</button>
                </form>
            </div>
        </div>
    </div>
@endsection


