@extends('layouts.app')

@section('content')
    <table class="table table-bordered col-4" style="margin: 5%">
        <tbody>
        <tr>
            @foreach($table->players as $player)
                <td>
                    <div class="container">
                            <div class="toast-header">
                                <strong class="mr-auto text-primary">{{ $player->code }}</strong>
                                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast"><a href="{{route('deletePlayer', [$player->id, $table->id])}}">&times;</a></button>
                            </div>
                            <div class="toast-body" id="player">
                                <input type="text" class="form-control mt-2" value="{{$player->first_name}}">
                            </div>
                    </div>
                </td>
            @endforeach
{{--            <form method="post" action="{{route('tables.update', [$table->id])}}">--}}
{{--                    @csrf--}}
{{--                    @method('PUT')--}}
{{--                    <td>--}}
{{--                        <input type="submit" class="btn btn-warning" value="update">--}}
{{--                    </td>--}}
{{--                </form>--}}
        </tr>
        </tbody>
    </table>
    <script>
        var x = document.getElementById("player");
        var y = document.getElementById("add");
        $(document).ready(function(){
            $("button").click(function(){
                $(x).replaceWith(function () {
                    return "<input type=\"submit\" class=\"btn btn-danger\" value=\"add\">"
                });
            });
        });
    </script>
@endsection
