@extends('layouts.app')

@section('content')
    @include('components.modalAddRound')
    <div class="container">
        <div class="row">
            <div class="col-1">
                <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
            </div>
            <div class="row col-5">
                <form method="post" action="{{route('storeTable', ['game' => $game->id, 'round' => $round->id])}}">
                    @csrf
                    <input type="submit" class="btn btn-primary m-3" value="Add players to tables randomly">
                </form>
            </div>
            <div class="row col-5 float-left">
                <button type="button" class="btn btn-secondary float-right m-3 shadow" data-toggle="modal"
                        data-target="#modalAddRound">Next Round
                </button>
            </div>
            <div class="col-1"></div>
            @foreach($tables as $table)
                <div class="col-6">
                    <div class="card m-3 shadow-lg p-4 bg-white">
                        <div class="card-header">
                            <div class="float-left">
                                Table: {{$table->number}}
                            </div>
                            <div class="float-right">
                                Total score: {{$table->points}}
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Score</th>
                                    <th scope="col">Weight</th>
                                    <th scope="col">Add Score</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table->players as $player)
                                    <tr>
                                        <th scope="row">{{$player->id}}</th>
                                        <td>{{$player->first_name}}</td>
                                        <td>{{$player->points}}</td>
                                        <td>{{number_format($player->weight, 2)}}</td>
                                        <div class="btn-group float-right">
                                            <td>
                                                <button type="button" class="btn-primary" data-toggle="modal"
                                                        data-target="#myModal{{$player->id}}">+
                                                </button>
                                            </td>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal{{$player->id}}" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Voer de score in</h4>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <form method="post"
                                                              action="{{route('storeScore', [$game->id, $round->id])}}">
                                                            @csrf
                                                            <input type="hidden" name="player_id" id="player_id"
                                                                   value="{{$player->id}}">
                                                            <div class="form-group col-md-12">
                                                                <label for="user_code">points</label>
                                                                <input class="form-control" type="number" name="points"
                                                                       required>
                                                            </div>
                                                            <div class="form-group col-md-8 mt-3">
                                                                <input type="submit" class="btn btn-primary"
                                                                       value="Verzend">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="btn-group float-right">
                                <form method="post" action="{{route('tables.destroy', [$table->id])}}">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger mr-1">delete</button>
                                </form>
                                <form method="get" action="{{route('tables.edit', [$table->id])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary">edit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <hr>
        <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
    </div>
@endsection


