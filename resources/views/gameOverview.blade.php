@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
            <h1 style="text-align: center;">Home Page Crew
                <a href="{{route('home')}}" class="btn btn-primary float-left" role="button">Back</a>
            </h1><hr>
            <div class="row">
            <div class="row h-100">
                <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card m-5 text-center card-hover" style="height: 4rem; background-color: #e3342f">
                            <div class="card-body">
                                <h5 style="color: white"><p>GAME RULES</h5>
                                <a href="{{route('game.gamerules.edit', ['game' => $game->id, 'gamerule' => $game->gamerules()->first()->id])}}" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card m-5 text-center card-hover" style="height: 4rem; background-color: #e3342f">
                            <div class="card-body">
                                <h5 style="color: white"><p>PLAYERS</h5>
                                <a href="{{route('game.player.index', ['game' => $game->id])}}" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="card m-5 text-center card-hover" style="height: 4rem; background-color: #e3342f">
                            <div class="card-body">
                                 <h5 style="color: white"><p>SCORE BOARD</h5>
                                <a href="{{route('scoresOverview', ['game' => $game->id])}}" class="stretched-link"></a>
                            </div>
                       </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card m-5 text-center card-hover" style="height: 4rem; background-color: #e3342f">
                            <div class="card-body">
                                <h5 style="color: white"><p>ROUNDS</h5>
                                <a href="{{route('showRounds', ['game' => $game->id])}}" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <img src="{{URL::asset('/images/Carcassonne.jpg')}}" alt="logo">
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
