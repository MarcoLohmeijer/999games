@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <h1>{{$msg}} </h1><hr>
           </div>
          <div class="col-md-2"></div>
      </div>
        <div class="row h-100">
            <div class="col-sm-2"></div>
            <div class="col-sm-2"><a href={{URL::previous()}} class="btn btn-primary float-left" > Back</a></div>
            <div class="col-sm-7">
                <img src="{{URL::asset('/images/joker.jpg')}}" alt="logo">
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
@endsection
