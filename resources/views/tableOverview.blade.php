@extends('layouts.app')

@section('content')
    <style>
        h1 {
            border-bottom: double;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="title m-b-md text-center p-3">
                    <h1>Overzicht tafelindeling ronde: ##</h1>
                </div>
            </div>
            <div class="col-sm-3"></div>
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="h-25 w-25 mb-2">
                        <div class="card">
                            <div class="card-header">Tafel: ##</div>
                            <div class="card-body">
                                <p>
                                    Spelerslijst<br>
                                    Marco Lohmeijer<br>
                                    Nick Meulenbroek<br>
                                    Fatih Topçu<br>
                                    Anastasiya Timofeyeva<br>
                                    Daniël Steeghs
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="h-25 w-25 mb-2">
                        <div class="card">
                            <div class="card-header">Tafel: ##</div>
                            <div class="card-body">
                                <p>
                                    Spelerslijst<br>
                                    Marco Lohmeijer<br>
                                    Nick Meulenbroek<br>
                                    Fatih Topçu<br>
                                    Anastasiya Timofeyeva<br>
                                    Daniël Steeghs
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="h-25 w-25 mb-2">
                        <div class="card">
                            <div class="card-header">Tafel: ##</div>
                            <div class="card-body">
                                <p>
                                    Spelerslijst<br>
                                    Marco Lohmeijer<br>
                                    Nick Meulenbroek<br>
                                    Fatih Topçu<br>
                                    Anastasiya Timofeyeva<br>
                                    Daniël Steeghs
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="h-25 w-25 mb-2">
                        <div class="card">
                            <div class="card-header">Tafel: ##</div>
                            <div class="card-body">
                                <p>
                                    Spelerslijst<br>
                                    Marco Lohmeijer<br>
                                    Nick Meulenbroek<br>
                                    Fatih Topçu<br>
                                    Anastasiya Timofeyeva<br>
                                    Daniël Steeghs
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="h-25 w-25 mb-2">
                        <div class="card">
                            <div class="card-header">Tafel: ##</div>
                            <div class="card-body">
                                <p>
                                    Spelerslijst<br>
                                    Marco Lohmeijer<br>
                                    Nick Meulenbroek<br>
                                    Fatih Topçu<br>
                                    Anastasiya Timofeyeva<br>
                                    Daniël Steeghs
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>

@endsection
