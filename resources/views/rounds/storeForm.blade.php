@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Backlogitem toevoegen</h1>
            <form action={{route('rounds.store')}} method="post">
                @csrf
                <div class="form-group">
                    <label>naam:</label>
                    <input name="name" type="text" class="form-control" id="name">
                </div>

                 <div class="form-group">
                    <label>time:</label>
                    <input name="time" type="number" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label>start_time:</label>
                    <input name="start_time" type="number" class="form-control" id="start_time">
                </div>

                <div class="form-group">
                    <label>game_id:</label>
                    <input name="game_id" type="number" class="form-control" id="game_id">
                </div>

                <div class="form-group">
                    <label>user_id:</label>
                    <input name="user_id" type="number" class="form-control" id="user_id">
                </div>

                <div class="form-group">
                    <button type="submit">submit</button>
                </div>


            </form>

        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
