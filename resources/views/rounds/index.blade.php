@extends('layouts.app')

@section('content')
    @include('components.modalAddRound')
    @include('components.modalAddKnockoutphase')
    <div class="row h-100">
        <div class="col-2"></div>
        <div class="col-8">
            <h1>Rounds
                <div class="btn-group float-right">
                    <button type="button" class="btn btn-secondary float-right m-3 shadow" data-toggle="modal"
                            data-target="#modalAddRound">Add Round
                    </button>
                </div>
                <div class="btn-group float-right">
                    <button type="button" class="btn btn-secondary float-right m-3 shadow" data-toggle="modal"
                            data-target="#modalAddKnockoutphase">Knock-out phase
                    </button>
                </div>
            </h1>
            <hr>
        </div>
        <div class="col-2"></div>
        <div class="col-2"></div>
        <div class="col-8">
            <div class="row h-100">
                @foreach($rounds as $round)
                    <div class="col-4">
                        <div class="card shadow text-center">
                            <div class="card-body">
                                Round: {{ $round->name }} <br>
                                Starting time {{ gmdate("H:i:s", $round->start_time) }}
                            </div>
                            <form method="get" action="{{route('showTables', [$game->id, $round->id])}}">
                                @csrf
                                <button type="submit" class="btn btn-primary shadow m-3 float-left">tables</button>
                            </form>
                            <form method="get" action="{{route('showClock', [$round->id])}}">
                                @csrf
                                <button type="submit" class="btn btn-primary shadow m-3 float-left">clock</button>
                            </form>
                        </div>
                    </div>
                @endforeach

            </div>{{$rounds->links()}}
            <hr>
            <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
        </div>

        <div class="col-2"></div>
    </div>
@endsection
