@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="mt-3">Scores</h1>
                <div class="row h-100">
                    @foreach($scores as $score)
                       <div class="col-md-8">
                           Points: {{$score->points}}
                       </div>
                       <div class="col-md-8">
                           Weight: {{$score->weight}}
                       </div>
                        <form method="get" action="{{route('scores.edit', [$score->id])}}">
                           @csrf
                           <input type="submit" class="btn btn-warning" value="update">
                       </form>
                        <form method="post" action="{{route('scores.destroy', [$score->id])}}">
                            @csrf
                            {{method_field('DELETE')}}
                            <input type="submit" class="btn btn-danger" value="delete" style="margin-left: 5px">
                        </form>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8" style="margin: 3%">
            <h1 class="mt-3">Details</h1>
                <div class="col-md-8 mt-3">
                    First name: {{$player->first_name}}
                </div>
                <div class="col-md-8">
                    Last name: {{$player->last_name}}
                </div>
                <div class="col-md-8">
                    Code: {{$player->code}}
                </div>
                <div class="col-md-8">
                    Phonenumber: {{$player->phonenumber}}
                </div>
                <div class="col-md-8">
                    Email: {{$player->email}}
                </div>
                <form method="get" action="{{route('player.edit', [$player->id])}}">
                    @csrf
                    <input type="submit" class="btn btn-warning" value="update">
                </form>
                <form method="post" action="{{route('player.destroy', [$player->id])}}">
                    @csrf
                    {{method_field('DELETE')}}
                    <input type="submit" class="btn btn-danger" value="delete" style="margin-left: 5px">
                </form>
            </div>
        </div>
    </div>
@endsection
