@extends('layouts.app')

@section('content')

    <div class="row h-100">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1 style="text-align: center;">Player Edit</h1><hr>
                    <form method="post" action="{{route('game.player.update', ['game' => $game->id, 'player' => $player->id])}}">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <input name="first_name" type="text" class="form-control mt-2" placeholder="First name"
                                   value="{{$player->first_name}}" required="required"/>
                            <input name="last_name" type="text" class="form-control mt-2" placeholder="Last name"
                                   value="{{$player->last_name}}" required="required"/>
                            <input name="email" type="email" class="form-control mt-2" placeholder="Email"
                                   value="{{$player->email}}" required="required"/>
                            <input name="phonenumber" type="number" class="form-control mt-2" placeholder="Phonenumber"
                                   value="{{$player->phonenumber}}" required="required"/>
                        </div>
                        <div class="modal-footer">
                            <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
                            <input type="submit" class="btn-warning" value="Update">
                        </div>
                    </form>

            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
@endsection
