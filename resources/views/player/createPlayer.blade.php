@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-1"></div>
                <div class="col-md-8" style="margin: 3%">
                    <h1>New Players Sign Up Form</h1><hr>
                    <form method="post" action="{{route('storePlayer', [$game->id])}}">
                        @csrf
                        <div class="modal-body">
                            <input name="first_name" type="text" class="form-control mt-2" placeholder="First name"
                                   required="required"/>
                            <input name="last_name" type="text" class="form-control mt-2" placeholder="Last name"
                                   required="required"/>
                            <input name="email" type="email" class="form-control mt-2" placeholder="Email"
                                   required="required"/>
                            <input name="phonenumber" type="number" class="form-control mt-2" placeholder="Phonenumber"
                                   required="required"/>
                        </div>
                        <div class="modal-footer">
                            <a href="{{ URL::previous() }}" class="btn btn-primary float-left" role="button">Back</a>
                            <input type="submit" class="btn-warning" value="Confirm">
                        </div>
                    </form>
                </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
@endsection
