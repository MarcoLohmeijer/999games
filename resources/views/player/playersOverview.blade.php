@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-9">
                <h1>Players Overview<a class="btn btn-primary btn float-right" href="{{route('game.player.create', ['game' => $game->id])}}">Create</a></h1><hr>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-2">
                            First Name
                        </div>
                        <div class="col-sm-2">
                            Last Name
                        </div>
                        <div class="col-sm-1">
                            Code
                        </div>
                        <div class="col-sm-2">
                            Phonenummer
                        </div>
                        <div class="col-sm-2">
                            E-mail
                        </div>
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-1">

                        </div>
                    </div><hr>
                    <div class="row">
                        @foreach($players as $player)

                            <div class="col-sm-2">
                                {{$player->first_name}}
                            </div>
                            <div class="col-sm-2">
                                {{$player->last_name}}
                            </div>
                            <div class="col-sm-1">
                                {{$player->code}}
                            </div>
                            <div class="col-sm-2">
                                {{$player->phonenumber}}
                            </div>
                            <div class="col-sm-2">
                                {{$player->email}}
                            </div>
                            <div class="col-1">
                                <button
                                    {{--data-path="{{ route('playerScore',[$player->id]) }}"--}}
                                    type="button" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#modalPlayerScores">Scores
                                </button>
                            </div>
                            <div class="col-sm-1">
                                <form method="get" action="{{route('game.player.edit', ['game' => $game->id, 'player' => $player->id])}}">
                                    @csrf
                                    <input type="submit" class="btn btn-warning" value="Update">
                                </form>
                            </div>
                            <div class="col-sm-1">
                                <form method="post" action="{{route('game.player.destroy', ['game' => $game->id, 'player' => $player->id])}}">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <input type="submit" class="btn btn-danger" value="Delete" style="margin-left: 5px">
                                </form>

                            </div><div class="col-sm-12"><hr></div><br><br>
                            <!-- The Modal -->
                            <div class="modal" id="modalPlayerScores">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            @foreach($rounds as $round)
                                                <hr>
                                                Round: {{$round->name}}
                                                <br>
                                                Game:{{$game->id}}

                                                @foreach($scores as $score)
                                                    @if($score->player_id === $player->id && $score->round_id === $round->id)
                                                        weight: {{$score->weight}}
                                                        <br>
                                                        points: {{$score->points}}
                                                    @endif
                                                @endforeach

                                            @endforeach

                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                            {{ $players->links() }}
                    </div>
                    <a href="{{route('game.overview', ['game' => $game->id])}}" class="btn btn-primary float-left" role="button">Back</a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
@endsection
