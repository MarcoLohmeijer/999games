@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">Choose a game to sign up</h1><hr>
                <div class="row h-100">
                    @foreach($games as $game)
                        <div class="col-md-4">
                            <div class="card w-100">
                                <div class="card-body">
                                    <h3>{{$game->name}}</h3>
                                    <hr>
                                    <p class="card-text">{{$game->description}}</p>
                                    <a href="{{route('game.player.create', ['game' => $game->id])}}" class="btn btn-primary">Choose Game</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection


