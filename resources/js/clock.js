document.getElementById("clock");

let remaining_time = null;
let targetTime = null;
let startTime = null;
let roundTime = null;
const url = window.location.href;
const id = url.split('/')[4];

//set time
let date = new Date();
let currentSeconds = date.getSeconds();
let currentMinutes = date.getMinutes();
let currentHours = date.getHours();
let currentUnix = Math.round(date.getTime() / 1000);

// get start-time from DB
function getStartTime() {
    $.ajax({
        type: 'GET',
        url: '/getOneRound/' + id.toString(),
        success: function(data){
            startTime= (data.round.start_time) ;
            roundTime= (data.round.time);
            setTargetTime();
        }
});}

getStartTime();

// set target-time
function setTargetTime(){
    return targetTime = startTime + roundTime;
}

// ads 0 in front of n if lower then 10
    function checkIfLowerThanTen(n) {
    if (n < 10) {
        return "0" + n.toString();
    } else {
        return n.toString();
    }
}
//timer loop
clock_interval = setInterval(function(){

    let date = new Date();
    let currentUnix = Math.round(date.getTime() / 1000);

    if (currentUnix < startTime){
        remaining_time =  startTime - currentUnix;
        document.getElementById("text-before").innerHTML = "ronde begint in:"

    }
    if (currentUnix > startTime ) {
        remaining_time = targetTime - currentUnix;
        document.getElementById("text-before").innerHTML = ""
    }
        let hours = Math.floor((remaining_time / 3600));
        let minutes = Math.floor((remaining_time / 60) % 60);
        let seconds = Math.floor((remaining_time % 60));


    if (currentUnix < targetTime){
        document.getElementById("clock").innerHTML = checkIfLowerThanTen(hours) + ":" + checkIfLowerThanTen(minutes)+":" + checkIfLowerThanTen(seconds)
    }

    if (remaining_time >= 0 ){
        document.getElementById("clock").innerHTML = checkIfLowerThanTen(hours) + ":" + checkIfLowerThanTen(minutes)+":" + checkIfLowerThanTen(seconds)
    }
    if (remaining_time <= 0){
        document.getElementById("text-before").innerHTML = "ronde afgelopen";
    }
}, 1000);
