<?php

return [
    'goBack' => 'Go back',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'show' => 'Show',
    'createNewGame' => 'Create new game',
    'create' => 'Create',
    'close' => 'Close'
];
