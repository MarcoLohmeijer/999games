<?php

return [
    'home' => [
        'title' => 'Games',
        'addNewGame' => 'Add new game',
        ''
    ],
    'formLabel' => [
        'name' => 'Name: ',
        'password' => 'Password: ',
        'email' => 'Email: ',
        'description' => 'Description: ',
    ]
];
